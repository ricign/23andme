import React from 'react'
import './Categories.css'

const Categories = ({list, onClick}) => (
	<section className="category">
    <header className="category__header"><h1>The New York Times Best Sellers</h1></header>
    <section className="category__list">
  	{
    	list.map( category => {
    			return <section key={category.list_name_encoded} className="subcategory">
                  <header className="subcategory__header">
											<a href="#" onClick={ () => onClick(category.list_name_encoded) }>
                        <h2>{ category.list_name }</h2>
                      </a>
                  </header>
                  <section className="subcategory__list">
                    <ol className="books">
                      { category.books.map( book => {
                        return <li className="books__item">
                                  <header>
                                    <img src={book.book_image} />
                                  </header>
                                  <article className="book">
                                    <p className="book__weeks">
                                      { book.weeks_on_list <= 1 ?
                                          `NEW THIS WEEK` :
                                          `${book.weeks_on_list} WEEKS ON THE LIST`
                                      }
                                    </p>
                                    <h3 className="book__title">{ book.title }</h3>
                                    <p className="book__author">{ book.contributor }</p>
                                    <p className="book__desc">{ book.description }</p>
                                  </article>
                                  <footer>
                                  </footer>
                               </li>
                      })}
                    </ol>
                  </section>
                </section>
  				})
  	}
    </section>
  </section>
)

export default Categories

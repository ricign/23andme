import React from 'react'
import './Books.css'

const Books = ({ list, onBack }) => (
	 <section className="list">
    <section className="list__bestseller"><a href="#" onClick={ onBack }>Best Seller</a></section>
    <header className="list__header"><h1>{list[0].list_name}</h1></header>
    <section>
      <ol className="booklist">
        {
          list.map( (book, index) => {
						const detail = book.book_details[0]
            return <li className="booklist__item">
                    <section className="booklist__number">{index+1}</section>
                    <article className="booklist__detail">
                      <p className="booklist__weeks">
                        { book.weeks_on_list <= 1 ?
                            `NEW THIS WEEK` :
                            `${book.weeks_on_list} WEEKS ON THE LIST`
                        }
                      </p>
                      <h3 className="booklist__title">{ detail.title }</h3>
                      <p className="booklist__author">
												{ detail.contributor } | { detail.publisher }
											</p>
                      <p className="booklist__desc">{ detail.description }</p>
                    </article>
                 </li>
          })
        }
      </ol>
    </section>
  </section>
)

export default Books

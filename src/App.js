import React, { Component } from 'react'
import Categories from './Categories'
import Books from './Books'
import './App.css'

class App extends Component {
  constructor() {
    super()

    this.state = {
      loading: true,
      result: null,
      category: null,
      categoryList: [],
      bookList: []
    }
  }

  componentWillMount() {
    fetch(`http://api.nytimes.com/svc/books/v3/lists/overview.json?api-key=a87d7b92aa73460faaed5a18c306578f`)
      .then( response => {
        if( response.status === 200) {
          return response.json()
        }
      })
      .then( json => {
        this.setState({
          loading: false,
          result: json.results,
          categoryList: json.results.lists
        })
      })
  }

  handleClick = (category) => {
    this.setState({
      loading: true
    })
    fetch(`https://api.nytimes.com/svc/books/v3/lists.json?api-key=a87d7b92aa73460faaed5a18c306578f&list=${category}`)
      .then( response => {
        if( response.status === 200) {
          return response.json()
        }
      })
      .then( json => {
        this.setState({
          loading: false,
          bookList: json.results,
          category
        })
      })
  }

  handleBack = () => {
    this.setState({
      category: null,
      bookList: []
    })
  }

  render() {
    const content = this.state.category ?
                      <Books
                        category={this.state.category}
                        list={this.state.bookList}
                        onBack={this.handleBack}
                      />
                    :
                      <Categories
                        list={this.state.categoryList}
                        onClick={this.handleClick}
                      />

    return (
      <div className="app">
        <div className="header">
          <img
            src="https://a1.nyt.com/assets/misc/20170504-122828/images/foundation/logos/nyt-logo-185x26.svg"
            width="185"
            height="26"
          />
        </div>
        <div className="content">
          { this.state.loading ? <div className="loader"></div> : content }
        </div>
      </div>
    );
  }
}

export default App;
